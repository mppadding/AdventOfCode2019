use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

fn vector_from_file(filename: impl AsRef<Path>) -> Vec<u32> {
    let file = File::open(filename).expect("No such file");
    let buf = BufReader::new(file);

    buf.lines()
        .nth(0)
        .unwrap()
        .unwrap()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect()
}

fn main() {
    let org: Vec<u32> = vec![
        1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 10, 1, 19, 1, 19, 9, 23, 1, 23, 6, 27,
        2, 27, 13, 31, 1, 10, 31, 35, 1, 10, 35, 39, 2, 39, 6, 43, 1, 43, 5, 47, 2, 10, 47, 51, 1,
        5, 51, 55, 1, 55, 13, 59, 1, 59, 9, 63, 2, 9, 63, 67, 1, 6, 67, 71, 1, 71, 13, 75, 1, 75,
        10, 79, 1, 5, 79, 83, 1, 10, 83, 87, 1, 5, 87, 91, 1, 91, 9, 95, 2, 13, 95, 99, 1, 5, 99,
        103, 2, 103, 9, 107, 1, 5, 107, 111, 2, 111, 9, 115, 1, 115, 6, 119, 2, 13, 119, 123, 1,
        123, 5, 127, 1, 127, 9, 131, 1, 131, 10, 135, 1, 13, 135, 139, 2, 9, 139, 143, 1, 5, 143,
        147, 1, 13, 147, 151, 1, 151, 2, 155, 1, 10, 155, 0, 99, 2, 14, 0, 0,
    ];
    let mut rom: Vec<u32>;
    let mut pc: usize = 0;
    let mut a;
    let mut b;
    let mut result;

    for noun in 0..=99 {
        for verb in 0..=99 {
            rom = org.clone();
            rom[1] = noun;
            rom[2] = verb;

            while rom[pc] != 99 {
                a = rom[rom[pc + 1] as usize];
                b = rom[rom[pc + 2] as usize];
                result = rom[pc + 3] as usize;

                match rom[pc] {
                    1 => rom[result] = a + b,
                    2 => rom[result] = a * b,
                    _ => {}
                }

                pc += 4;
            }

            pc = 0;
            if rom[0] == 19690720 {
                println!("noun: {}, verb: {}", noun, verb);
            }
        }
    }
}
