
def matches_criteria(num):
    if len(num) != 6:
        return False

    for x in range(0, 5):
        if int(num[x]) - int(num[x+1]) > 0:
            return False

    it = 0

    groups = 0

    while it != 6:
        size = 1
        char = num[it]

        for x in range(1, 6 - it):
            if num[x + it] == char:
                size += 1
            else:
                break

        if size == 2:
            groups += 1

        it += size

    return groups > 0


t = 0

for x in range(152085, 670283):
    if matches_criteria(str(x)):
        t += 1

print(t)
