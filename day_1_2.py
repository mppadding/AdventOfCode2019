import math

file = open("input/day_1.in", "r")

sum = 0

for l in file:
    fuel = math.floor(int(l) / 3) - 2
    prev = fuel

    while 1:
        prev = math.floor(prev / 3) - 2
        if prev <= 0:
            break

        fuel += prev
    
    sum += fuel

print(sum)
