import copy, math

def simulate_step(pos, vel):
    for a in range(len(vel)):
        for b in range(a, len(vel)):
            for axis in range(len(pos[0])):
                vel[a][axis] += 1 if pos[a][axis] < pos[b][axis] else (0 if pos[a][axis] == pos[b][axis] else -1)
                vel[b][axis] += 1 if pos[b][axis] < pos[a][axis] else (0 if pos[a][axis] == pos[b][axis] else -1)

    for i in range(len(vel)):
        for axis in range(len(pos[0])):
            pos[i][axis] += vel[i][axis]

    return (pos, vel)


def planet_print(pos, vel):
    for i in range(len(pos)):
        print("{0}: pos=<x={1}, y={2}, z={3}>, vel=<x={4}, y={5}, z={6}>".format(i, pos[i][0], pos[i][1], pos[i][2], vel[i][0], vel[i][1], vel[i][2]))

def simulate(pos, vel, steps):
    step_num = 0
    while step_num < steps:
        (pos, vel) = simulate_step(pos, vel)
        step_num += 1

    return (step_num, calculate_energy(pos, vel))

def run(pos, vel):
    orig_pos, orig_vel = pos[:], vel[:]
    step_num = 0

    while not step_num or orig_pos != pos or orig_vel != vel:
        for a in range(len(vel)):
            for b in range(a, len(vel)):
                vel[a] += 1 if pos[a] < pos[b] else (0 if pos[a] == pos[b] else -1)
                vel[b] += 1 if pos[b] < pos[a] else (0 if pos[a] == pos[b] else -1)

        for i in range(len(pos)):
            pos[i] += vel[i]

    return step_num


def simulate_till_same(pos, vel):
    orig_pos, orig_vel = copy.deepcopy(pos), copy.deepcopy(vel)
    (pos, vel) = simulate_step(pos, vel)
    step_num = 1

    while orig_pos != pos or orig_vel != vel:
        (pos, vel) = simulate_step(pos, vel)
        step_num += 1

    return step_num

def calculate_energy(pos, vel):
    total = 0
    for i in range(len(pos)):
        total += sum(list(map(abs, pos[i]))) * sum(list(map(abs, vel[i])))

    return total

def lcm(a, b):
    return a * b // math.gcd(a, b)

def part2(pos):
    steps_x = run([x for x, _, _ in pos], [0] * len(pos))
    steps_y = run([y for _, y, _ in pos], [0] * len(pos))
    steps_z = run([z for _, _, z in pos], [0] * len(pos))

    return lcm(lcm(steps_x, steps_y), steps_z)

pos = [
        [-3, 10, -1],
        [-12, -10, -5],
        [-9, 0, 10],
        [7, -5, -3]
]

vel = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
]

(num, energy) = simulate(pos, vel, 1000)

print(f"Step num: {num}, energy: {energy}")

pos = [
        [-3, 10, -1],
        [-12, -10, -5],
        [-9, 0, 10],
        [7, -5, -3]
]

vel = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
]
