import math

def setup_map(lines):
    width = len(lines[0])
    height = len(lines)
    asteroids = []

    for y in range(width):
        for x in range(height):
            if lines[y][x] == '#':
                asteroids.append((x, y))

    return asteroids

def distance(a, b):
    dy = abs(a[1] - b[1])
    dx = abs(a[0] - b[0])
    return math.sqrt(dx*dx + dy*dy)

def los(asteroids, source, destination):
    if source == destination:
        return False

    total_distance = distance(source, destination)

    # Bound conditions
    x = sorted([destination[0], source[0]])
    y = sorted([destination[1], source[1]])

    for asteroid in asteroids:
        # Same as source or destination
        if asteroid == source or asteroid == destination:
            continue

        # X out of bounds
        if asteroid[0] < x[0] or asteroid[0] > x[1]:
            continue

        # Y out of bounds
        if asteroid[1] < y[0] or asteroid[1] > y[1]:
            continue

        # Since floating point cant always be trusted, if a difference of less than 1 PPM we consider them equal.
        if abs(total_distance - (distance(source, asteroid) + distance(destination, asteroid))) < 0.000001:
            return False

    return True

def total_los(asteroids, source):
    total = 0
    for asteroid in asteroids:
        total += 1 if los(asteroids, source, asteroid) else 0

    return total

def get_best_asteroid(asteroids):
    total = 0
    pos = (0, 0)

    for asteroid in asteroids:
        curr_los = total_los(asteroids, asteroid)
        if curr_los > total:
            total = curr_los
            pos = asteroid

    return (pos, total)

raw = r"""
.............#..#.#......##........#..#
.#...##....#........##.#......#......#.
..#.#.#...#...#...##.#...#.............
.....##.................#.....##..#.#.#
......##...#.##......#..#.......#......
......#.....#....#.#..#..##....#.......
...................##.#..#.....#.....#.
#.....#.##.....#...##....#####....#.#..
..#.#..........#..##.......#.#...#....#
...#.#..#...#......#..........###.#....
##..##...#.#.......##....#.#..#...##...
..........#.#....#.#.#......#.....#....
....#.........#..#..##..#.##........#..
........#......###..............#.#....
...##.#...#.#.#......#........#........
......##.#.....#.#.....#..#.....#.#....
..#....#.###..#...##.#..##............#
...##..#...#.##.#.#....#.#.....#...#..#
......#............#.##..#..#....##....
.#.#.......#..#...###...........#.#.##.
........##........#.#...#.#......##....
.#.#........#......#..........#....#...
...............#...#........##..#.#....
.#......#....#.......#..#......#.......
.....#...#.#...#...#..###......#.##....
.#...#..##................##.#.........
..###...#.......#.##.#....#....#....#.#
...#..#.......###.............##.#.....
#..##....###.......##........#..#...#.#
.#......#...#...#.##......#..#.........
#...#.....#......#..##.............#...
...###.........###.###.#.....###.#.#...
#......#......#.#..#....#..#.....##.#..
.##....#.....#...#.##..#.#..##.......#.
..#........#.......##.##....#......#...
##............#....#.#.....#...........
........###.............##...#........#
#.........#.....#..##.#.#.#..#....#....
..............##.#.#.#...........#.....
"""

raw = raw.split()
asteroids = setup_map(raw)
(part1_pos, part1_los) = get_best_asteroid(asteroids)

print(f'Best position: {part1_pos}, can view {part1_los} asteroids')
