import math

file = open("input/day_1.in", "r")

sum = 0

for l in file:
    sum += (math.floor(int(l) / 3) - 2)

print(sum)
