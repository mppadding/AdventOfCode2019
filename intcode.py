class Opcode:
    code, num_args = 0, 0

    def execute(self, intcode, args):
        pass

    def calc_pc(self, intcode):
        return intcode.pc + self.num_args + 1

class AdditionOpcode(Opcode):
    code, num_args = 1, 3

    def execute(self, intcode, args):
        intcode.set(args[2].addr, args[0].val + args[1].val)
        return self.calc_pc(intcode)

class MulitplicationOpcode(Opcode):
    code, num_args = 2, 3

    def execute(self, intcode, args):
        intcode.set(args[2].addr, args[0].val * args[1].val)
        return self.calc_pc(intcode)

class InputOpcode(Opcode):
    code, num_args = 3, 1

    def execute(self, intcode, args):
        intcode.set(args[0].addr, intcode.get_input())
        return self.calc_pc(intcode)

class OutputOpcode(Opcode):
    code, num_args = 4, 1

    def execute(self, intcode, args):
        intcode.output = args[0].val
        return self.calc_pc(intcode)

class JumpIfTrueOpcode(Opcode):
    code, num_args = 5, 2

    def execute(self, intcode, args):
        return args[1].val if args[0].val != 0 else self.calc_pc(intcode)

class JumpIfFalseOpcode(Opcode):
    code, num_args = 6, 2

    def execute(self, intcode, args):
        return args[1].val if args[0].val == 0 else self.calc_pc(intcode)

class LessThanOpcode(Opcode):
    code, num_args = 7, 3

    def execute(self, intcode, args):
        intcode.set(args[2].addr, int(args[0].val < args[1].val))
        return self.calc_pc(intcode)

class EqualsOpcode(Opcode):
    code, num_args = 8, 3

    def execute(self, intcode, args):
        intcode.set(args[2].addr, int(args[0].val == args[1].val))
        return self.calc_pc(intcode)

class RelativeBaseOffsetOpcode(Opcode):
    code, num_args = 9, 1

    def execute(self, intcode, args):
        intcode.relative_base += args[0].val
        return self.calc_pc(intcode)

class HaltOpcode(Opcode):
    code, num_args = 99, 0

    def execute(self, intcode, args):
        intcode.halted = True
        return None


class Argument:
    def __init__(self, val, addr):
        self.val = val
        self.addr = addr

class IntCode:
    def __init__(self, rom, inputs=[]):
        self.rom = rom
        self.inputs = inputs[::-1]
        self.output = None

        self.pc = 0
        self.relative_base = 0

        self.halted = False;

        self.ram = {}

    def _get_opcode(self, opcode):
        #print(opcode)
        return next(cls for cls in Opcode.__subclasses__() if cls.code == opcode)

    def _get_arguments(self, num_args):
        args = []
        modes = str(self.rom[self.pc]).zfill(5)[:3][::-1]

        for i in range(num_args):
            val = self.rom[self.pc+i+1] + (self.relative_base if modes[i] == '2' else 0)

            if modes[i] == '1':
                args.append(Argument(val, val))
            else:
                args.append(Argument(self.rom[val] if val < len(self.rom) else self.ram.get(val, 0), val))

        return args

    def set(self, address, value):
        target = self.rom if address < len(self.rom) else self.ram
        target[address] = value;

    def get_input(self):
        return self.inputs.pop()

    def input(self, value):
        self.inputs = [value] + self.inputs

    # Runs till halted or first output
    def run(self):
        self.output = None
        while not self.halted and self.output is None:
            opcode = self._get_opcode(self.rom[self.pc] % 100)
            args = self._get_arguments(opcode.num_args)
            self.pc = opcode().execute(self, args)

        return self.output

    # Runs till halted
    def execute(self):
        last_output = None
        while not self.halted:
            output = self.run()

            if not self.halted:
                last_output = output

        return last_output
